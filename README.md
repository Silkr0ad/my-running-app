# My Running App

A barebones distance tracker.

<p align="center">
    <img src="Media/screenshot.jpg" height="540" />
</p>


## Goals

1. ~~Finish up the Wings for Life functionality.~~
2. Add a distance-timing function, with alerts via sound and/or vibration, such as for 400 m, 1k, 1 mi, 5k, etc.
3. Add a "basic" run mode à la Samsung Health's.
4. Add a way to keep the app running in the background (/w Android Services).

## Attributions

All external assets will be credited below.

### Art

- (Temporary) background image: **TODO** add the link to the Walli author
- The moon photo used in own, created background image: https://hdqwalls.com/crescent-moon-night-sky-5k-wallpaper
- (Temporary) app icon: from flaticons?

### Sound

None yet.

### Code

- Distance calculation from latitude and longitude: https://stackoverflow.com/a/11172685/7151327