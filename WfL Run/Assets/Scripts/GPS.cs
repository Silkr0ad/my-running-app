﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.UI;

public class GPS : MonoBehaviour {
    public Text locationData;
    public Text buttonData;
    public Text debugData;
    public Text accuracyData;
    public Text updateData;

    public float latitude, longitude;
    public float prevLat, prevLon;
    public float totalDistance;

    private LocationService loc;

    private int toggleIndex = 0;

    private void Awake() {
        loc = Input.location;
    }

    private void Start() {
        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void Update() {
        if (loc.status == LocationServiceStatus.Running) {
            prevLat = latitude;
            prevLon = longitude;

            latitude = loc.lastData.latitude;
            longitude = loc.lastData.longitude;

            if (prevLat != 0 && prevLon != 0)
                totalDistance += Mathf.Abs(GetDeltaDistance(latitude, longitude, prevLat, prevLon));

            locationData.text = "latitude: " + latitude.ToString() + "\n" + "longitude: " + longitude.ToString() +
            "\n\n" + "distance: " + ((int)totalDistance).ToString() + " m";
        }
    }

    public void ToggleGPS() {
        if (toggleIndex % 2 == 0) {
            StartGPS();
            buttonData.text = "Stop GPS";
        } else {
            StopGPS();
            buttonData.text = "Start GPS";
        }
        toggleIndex++;
    }

    public void Clear() {
        latitude = 0;
        longitude = 0;
        prevLat = 0;
        prevLon = 0;
        totalDistance = 0;
        locationData.text = "latitude: 0" + "\n" + "longitude: 0" + "\n\n" + "distance: 0 m";
        debugData.text = "";
    }

    private void StartGPS() {
        StartCoroutine(StartLocationServices());
    }

    private void StopGPS() {
        loc.Stop();
        if (loc.status == LocationServiceStatus.Stopped)
            SetDebugMessage("GPS stopped successfully.");
        else
            SetDebugMessage("GPS failed to stop.");
    }

    private void SetDebugMessage(string msg) {
        debugData.text = msg;
    }

    public float GetDeltaDistance(float lat1, float lon1, float lat2, float lon2) {
        float R = 6378.137f; // Radius of earth in KM
        float dLat = lat2 * Mathf.PI / 180 - lat1 * Mathf.PI / 180;
        float dLon = lon2 * Mathf.PI / 180 - lon1 * Mathf.PI / 180;
        float a = Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) +
        Mathf.Cos(lat1 * Mathf.PI / 180) * Mathf.Cos(lat2 * Mathf.PI / 180) *
        Mathf.Sin(dLon / 2) * Mathf.Sin(dLon / 2);
        float c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        float d = R * c;
        return d * 1000f; // meters
    }

    private IEnumerator StartLocationServices() {
        Permission.RequestUserPermission(Permission.FineLocation);

        // check if the user has enabled locations services
        if (!loc.isEnabledByUser) {
            SetDebugMessage("GPS not enabled by user.");
            //yield break;
        }

        // start location services and wait until it's on
        float accuracy = 0f, update = 0f;
        if (accuracyData.text != "")
            float.TryParse(accuracyData.text, out accuracy);
        if (updateData.text != "")
            float.TryParse(updateData.text, out update);
        
        if (accuracy == 0f) accuracy = 0.1f;
        if (update == 0f) update = 3f;
        
        Input.location.Start(accuracy, update);

        int maxWait = 20;
        while (loc.status == LocationServiceStatus.Initializing && maxWait > 0) {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // if the attempt has timed out, break
        if (maxWait <= 0) {
            SetDebugMessage("GPS timed out.");
            yield break;
        } else {
            SetDebugMessage("GPS started successfully." +
                "\nAccuracy: " + accuracy.ToString() + " | Update: " + update.ToString());
        }
    }
}
