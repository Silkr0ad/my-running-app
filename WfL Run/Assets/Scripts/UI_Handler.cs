﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Handler : MonoBehaviour {
    public Button stopButton;
    public Button clearButton;

    public RawImage lockImage;

    private int i = 0;

    public void ToggleEnabled() {
        if (i % 2 == 0) {
            stopButton.interactable = false;
            clearButton.interactable = false;
            lockImage.rectTransform.rotation = Quaternion.Euler(0f, 0f, -20f);
        } else {
            stopButton.interactable = true;
            clearButton.interactable = true;
            lockImage.rectTransform.rotation = Quaternion.Euler(0f, 0f, 0f);
        }
        i++;
    }
}
