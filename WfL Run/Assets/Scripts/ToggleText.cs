﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleText : MonoBehaviour {
    public Text txt;
    public List<string> labels;
    private int i;

    public void Toggle() {
        txt.text = labels[i % labels.Count];
        i++;
    }
}
